@IsTest
public with sharing class LS_cls_communityAlm_test {

	public static testMethod void testCommunityAlmDemo(){
		insert new LS_obj_demo__c(Name='Test Demo', LS_fld_type__c='ALM');

		Test.startTest();

		LS_obj_demo__c[] demos = LS_cls_communityAlm_cc.getAlmContent();
		System.assertEquals(1, demos.size());

		Test.stopTest();
	}

	public static testMethod void testCommunityInnovationDemo(){
		insert new LS_obj_demo__c(Name='Test Demo', LS_fld_type__c='Innovation');

		Test.startTest();

		LS_obj_demo__c[] demos = LS_cls_innovationDemo_cc.getDemos();
		System.assertEquals(1, demos.size());

		Test.stopTest();
	}

	public static testMethod void testLightningComponentCommunity(){
		insert new LS_Images_Lightning_Components__c(Name='Test Demo');

		Test.startTest();

		LS_Images_Lightning_Components__c[] components = LS_cls_LightnignComponentCommunity_cc.getComponents(); 
		System.assertEquals(1, components.size());

		Test.stopTest();
	}
	public static testMethod void testNews(){
		insert new Liquid_Studio_News__c(Name='Test Demo',Title__c='Test Demo', News_Content__c='Test Demo', Image_Source__c='Test Demo');

		Test.startTest();

		Liquid_Studio_News__c[] news = LS_cls_news_cc.getNews();
		System.assertEquals(1, news.size());

		Test.stopTest();
	}
}