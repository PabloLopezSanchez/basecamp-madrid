public class EinsteinDisplayer {
    
    class WrapperReturnJSON {
        public String image {get;set;}
        public String prediction {get;set;}
    }

    
    @AuraEnabled
    public static String getImageAndPrediction() {
        WrapperReturnJSON retObj = new WrapperReturnJSON();
        String query = null;

        // SELECT <Humidity__c | Temperature__c> FROM RoomStats__c ORDER BY CreatedDate DESC LIMIT 1
        query = 'SELECT Humidity__c, Temperature__c FROM RoomStats__c ORDER BY CreatedDate DESC LIMIT 1';

        if(query != null) {
            try {
                List<SOBject> lstObj = Database.query(query);
                retObj.image = String.valueOf(lstObj[0].get('Humidity__c'));
                retObj.prediction = String.valueOf(lstObj[0].get('Temperature__c'));
                return JSON.serialize(retObj);
            } catch(Exception ignored) {}
        }
        return null;
    }

}