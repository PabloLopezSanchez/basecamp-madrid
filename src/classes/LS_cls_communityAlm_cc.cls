public class LS_cls_communityAlm_cc {

	@AuraEnabled
	public static List<LS_obj_demo__c> getAlmContent() {
		// Retrieves all the ALM Components
        return [SELECT Name, LS_fld_title__c, LS_fld_description__c, LS_fld_urlBlog__c, LS_fld_component__c
				FROM LS_obj_demo__c WHERE LS_fld_type__c = 'ALM'];
    }
}