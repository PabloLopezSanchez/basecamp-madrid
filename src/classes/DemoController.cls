public with sharing class DemoController {
    /**
     * An empty constructor for the tsesting
     */
    public DemoController() {
        System.debug('Salesforce Dx and Heroku Flow');
    }

    /**
     * Get the version of the SFDX demo app
     */
    public String getAppVersion() {
        return '1.0.0';
    }
}