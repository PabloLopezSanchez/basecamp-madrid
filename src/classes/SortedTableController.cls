public class SortedTableController {
    public List<Case> cases {get; set;}
    
    public SortedTableController() {
        cases = [SELECT Id, CaseNumber, CreatedBy.Name, Description FROM Case LIMIT 20];
    }
    
}