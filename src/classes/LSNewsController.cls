public with sharing class LSNewsController {

    @AuraEnabled
    public static List<Liquid_Studio_News__c> getNews() {
        return (List<Liquid_Studio_News__c>)[SELECT Id, Name, Title__c, News_Content__c, Image_Source__c, Blog_URL__c, CreatedDate FROM Liquid_Studio_News__c ORDER BY CreatedDate DESC];
    }
}