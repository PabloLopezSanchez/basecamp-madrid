public with sharing class LS_cls_innovationDemo_cc {
    /* Metodo para obtener las LS_obj_demo__c */
    @AuraEnabled
    public static List<LS_obj_demo__c> getDemos() {
        return (List<LS_obj_demo__c>)[SELECT Id, LS_fld_title__c, LS_fld_imageSource__c, LS_fld_urlBlog__c, LS_fld_videoSourcePreview__c FROM LS_obj_demo__c WHERE LS_fld_type__c = 'Innovation'];
    }
}