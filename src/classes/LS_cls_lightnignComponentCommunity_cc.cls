public with sharing class LS_cls_lightnignComponentCommunity_cc {
   @AuraEnabled
    public static List<LS_Images_Lightning_Components__c> getComponents() {
        return (List<LS_Images_Lightning_Components__c>)[SELECT Id, Name, Tittle__c, Image_Source__c,
														 LS_fld_urlBlog__c FROM LS_Images_Lightning_Components__c ];
    }
}