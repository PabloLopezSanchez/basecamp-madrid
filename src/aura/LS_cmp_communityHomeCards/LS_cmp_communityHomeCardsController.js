({
	doInit: function(component, event, helper) {
		console.log(component);
		window.setInterval(
			function() {
			   var scrollTop = document.documentElement.scrollTop || document.body.scrollTop;
			   if (scrollTop >= 0 && scrollTop < 100) {
					$A.util.addClass(component.find("cloud1"), 'cloud-1-animated-in');
					$A.util.addClass(component.find("cloud2"), 'cloud-2-animated-in');
					$A.util.removeClass(component.find("cloud1"), 'cloud-1-animated-out');
					$A.util.removeClass(component.find("cloud2"), 'cloud-2-animated-out');
			   } else if (scrollTop >= 100) {
					$A.util.removeClass(component.find("cloud1"), 'cloud-1-animated-in');
					$A.util.removeClass(component.find("cloud2"), 'cloud-2-animated-in');
					$A.util.addClass(component.find("cloud1"), 'cloud-1-animated-out');
					$A.util.addClass(component.find("cloud2"), 'cloud-2-animated-out');
			   }
		   }
		   ,1000); 
	}
})