({
	skipIntro: function (component, event) {
		var intro = component.find("presentation");
		var header = component.find("element0");
		$A.util.removeClass(header, 'slds-hide');
		$A.util.addClass(intro, 'fadeOut');
		$A.util.addClass(intro, 'slds-hide');
		if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
			component.set("v.currentComponent", 0);
			this.scrollDown(component, event);
		} else {
			$A.util.removeClass(header, 'slds-hide');
			component.set("v.currentComponent", 0);
		}
	},
	scrollUp: function (component, event) {
		var nComponent = component.get("v.currentComponent");
		if ((!/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) &&
				nComponent > 0 && nComponent <= 9) ||
			(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) &&
				nComponent > 1)) {
			var prev = nComponent - 1;
			console.log(prev);
			var elementToShow = component.find("element" + prev);
			var elementTohide = component.find("element" + nComponent);
			$A.util.removeClass(elementToShow, 'slds-hide');
			$A.util.addClass(elementTohide, 'slds-hide');
			component.set("v.currentComponent", prev);
		}
	},
	scrollDown: function (component, event) {
		var nComponent = component.get("v.currentComponent");
		var next = nComponent + 1;
		console.log(next);
		if (nComponent >= 0 && nComponent <= 8) {
			var elementToShow = component.find("element" + next);
			console.log(elementToShow);
			var elementTohide = component.find("element" + nComponent);
			console.log(elementTohide);
			$A.util.removeClass(elementToShow, 'slds-hide');
			$A.util.addClass(elementTohide, 'slds-hide');
			component.set("v.currentComponent", next);
		}
	},
	playVideo: function (component, event) {
		if (component.find("videoLS").getElement()) {
			var video = component.find("videoLS").getElement().firstChild;
			video.play();
		}
	}
})