({
	skipIntro : function(component, event, helper){
		helper.skipIntro(component, event);
		helper.playVideo(component, event);
	},	
	scrollUp : function(component, event, helper){
		helper.scrollUp(component, event);
	},
	scrollDown : function(component, event, helper){
		helper.scrollDown(component, event);
	}
})