({
	playVideo : function(component, event, helper) {
		console.log(event.getSource());
		var isPlay = component.get("v.playPause");
		if(component.find("videoLS").getElement() && isPlay=='utility:right'){
			var video = component.find("videoLS").getElement().firstChild;
			video.play();
			component.set("v.playPause", "utility:pause");
		}
		else if(component.find("videoLS").getElement() && isPlay=='utility:pause'){
			var video = component.find("videoLS").getElement().firstChild;
			video.pause();
			component.set("v.playPause", "utility:right");
		}
	},
	doInit : function(component, event, helper){
		var isChrome = !!window.chrome && !!window.chrome.webstore;
		component.set("v.isChrome", isChrome);
	}
})