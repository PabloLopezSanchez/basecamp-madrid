({
	handleShowModal: function (component, evt, helper) {
		var componentName = component.get('v.component')
		var modalBody;
		console.log(componentName)
		$A.createComponent(componentName, {},
			function (content, status) {
				if (status === "SUCCESS") {
					modalBody = content;
					component.find('overlayLib').showCustomModal({
						body: modalBody,
						showCloseButton: true,
						cssClass: "slds-modal",
						closeCallback: function () {
							console.log('Modal Closed')
						}
					})
				}
			}
		);
	}
})