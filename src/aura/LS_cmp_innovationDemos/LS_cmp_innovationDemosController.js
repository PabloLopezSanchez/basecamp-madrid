({
serverGetDemos: function (component, event, helper) {
	//create server side action
	var action = component.get("c.getDemos");
	action.setCallback(this, function (response) {
		var state = response.getState();
		if (state === "SUCCESS") {
			var data = response.getReturnValue();
			console.log(data);
			component.set("v.innovationDemos", data);	
			if((typeof window.orientation !== "undefined") || (navigator.userAgent.indexOf('IEMobile') !== -1) || /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) { 
				component.set("v.isMobile", true);
			}
		} else if (state === "INCOMPLETE") {
			// do something
		} else if (state === "ERROR") {
			var errors = response.getError();
			if (errors) {
				if (errors[0] && errors[0].message) {
					console.log("Error message: " +
						errors[0].message);
				}
			} else {
				console.log("Unknown error");
			}
		}
	});
	$A.enqueueAction(action);
},
playVideo: function(component, event, helper){
	console.log(event.srcElement.play());
},
pauseVideo: function(component, event, helper){
	console.log(event.srcElement.pause());
},
checkMobile: function(component){
// device detection
if((typeof window.orientation !== "undefined") || (navigator.userAgent.indexOf('IEMobile') !== -1)) { 
    component.set("v.isMobile", true);
}
}


})