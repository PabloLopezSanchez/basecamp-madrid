({
    retrieveAlmComponents : function(component, event, helper) {
		var action = component.get("c.getAlmContent");
		action.setCallback(this, function (response) {
			var state = response.getState();
			if (state == 'SUCCESS') {
				component.set("v.almComponents", response.getReturnValue())
			} else {
				console.error('Could not connect to controller')
			}
		});
		$A.enqueueAction(action);
	}
})