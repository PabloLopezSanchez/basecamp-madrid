({
	doInit : function(component, event, helper){
		var isChrome = !!window.chrome && !!window.chrome.webstore;
		component.set("v.isChrome", isChrome);
	},
	playVideo : function(component, event, helper) {
		if(component.find("videoLS").getElement()){
			var video = component.find("videoLS").getElement().firstChild;
			video.play();
		}
	}
})